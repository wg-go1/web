package model

import "time"

type WgConfig struct {
	Plan   Plan
	Name   string
	Config string
	DateTo time.Time
}

type Plan string

const Common Plan = "common"
const Rare Plan = "rare"
const Epic Plan = "epic"
const Legendary Plan = "legendary"

func ToPlan(s string) Plan {
	if s == string(Common) {
		return Common
	}
	if s == string(Rare) {
		return Rare
	}
	if s == string(Epic) {
		return Epic
	}
	if s == string(Legendary) {
		return Legendary
	}

	return ""
}

type PlanConfig struct {
	DurationMin int `json:"minutes"`
	Limit       int `json:"limit"`
}

type PlanConfigs map[Plan]PlanConfig

var PlanConfigOptions = PlanConfigs{
	Common: {DurationMin: 15, Limit: 20},
}

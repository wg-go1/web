package main

import (
	"context"
	"log"
	"time"
	"wg-go-web/config"
	"wg-go-web/handler"
	"wg-go-web/storage"

	firebase "firebase.google.com/go"
	"github.com/go-redis/redis/v8"
	"github.com/goombaio/namegenerator"
	"github.com/labstack/echo/v4"
	"google.golang.org/api/option"
)

func main() {
	ctx := context.Background()
	e := echo.New()
	cfg := config.LoadConfig(e.Logger)

	seed := time.Now().UTC().UnixNano()
	nameGenerator := namegenerator.NewNameGenerator(seed)

	opt := option.WithCredentialsFile("../config/wg-vpn-382510-firebase-adminsdk-8kh4z-a9a51617e3.json")
	firebaseApp, err := firebase.NewApp(ctx, nil, opt)
	if err != nil {
		log.Fatalf("error initializing app: %v", err)
	}

	firebaseAuth, err := firebaseApp.Auth(ctx)
	if err != nil {
		log.Fatalf("error getting Auth client: %v\n", err)
	}

	redisClient := redis.NewClient(&redis.Options{
		Addr:     cfg.AppRedis,
		Password: cfg.AppRedisPass,
	})
	defer redisClient.Close()

	_, err = redisClient.Ping(ctx).Result()
	if err != nil {
		log.Fatalf("redis ping failed %v\n", err)
	}

	storage := storage.New(redisClient, e.Logger)
	handler := handler.New(storage, firebaseAuth, e.Logger, nameGenerator, cfg)

	e.GET("/welcome", handler.Welcome, handler.VerifyToken)
	e.GET("/newPeer", handler.NewPeer, handler.VerifyToken)
	e.Static("/", "frontend")
	e.Logger.Fatal(e.Start(":" + cfg.AppHttpPort))
}

package config

import (
	"os"

	"github.com/joho/godotenv"
	"github.com/labstack/echo/v4"
)

type Config struct {
	AppRedis     string
	AppRedisPass string
	AppHttpPort  string

	CoreAuthKey string
	CoreAddr    string
}

func LoadConfig(logger echo.Logger) *Config {
	err := godotenv.Load("../config/.env")
	if err != nil {
		logger.Fatalf("error loading config: %v", err)
	}

	appRedis := os.Getenv("APP_REDIS")
	if appRedis == "" {
		logger.Fatalf("missing APP_REDIS environment variable")
	}

	appRedisPass := os.Getenv("APP_REDIS_PASS")

	appHttpPort := os.Getenv("APP_HTTP_PORT")
	if appHttpPort == "" {
		logger.Fatalf("missing APP_HTTP_PORT environment variable")
	}

	coreAuthKey := os.Getenv("CORE_AUTH_KEY")
	if coreAuthKey == "" {
		logger.Fatalf("missing CORE_AUTH_KEY environment variable")
	}

	coreAddr := os.Getenv("CORE_ADDR")
	if coreAddr == "" {
		logger.Fatalf("missing CORE_ADDR environment variable")
	}

	return &Config{
		AppRedis:     appRedis,
		AppRedisPass: appRedisPass,
		AppHttpPort:  appHttpPort,
		CoreAuthKey:  coreAuthKey,
		CoreAddr:     coreAddr,
	}
}

import { initializeApp } from "https://www.gstatic.com/firebasejs/9.6.9/firebase-app.js";
import { getAuth, GoogleAuthProvider, signInWithPopup, signOut, onAuthStateChanged } from "https://www.gstatic.com/firebasejs/9.6.9/firebase-auth.js";

const app = initializeApp({
  apiKey: "AIzaSyDr5QYK9qCMETa1rH4e9bRXt5zrLKRekok",
  authDomain: "wg-vpn-382510.firebaseapp.com",
  projectId: "wg-vpn-382510",
  storageBucket: "wg-vpn-382510.appspot.com",
  messagingSenderId: "356294040739",
  appId: "1:356294040739:web:9e0248d60016c0b9f33316"
});
const provider = new GoogleAuthProvider(app);
provider.setCustomParameters({ prompt: "select_account" });
const auth = getAuth(app);

MicroModal.init({
  openTrigger: 'data-custom-open',
  closeTrigger: 'data-custom-close',
  openClass: 'is-open',
  disableScroll: true,
  disableFocus: false,
  awaitOpenAnimation: false,
  awaitCloseAnimation: false,
  debugMode: true
});

new ClipboardJS('button');

const welcomePage = document.getElementById("welcomePage");
const loginPage = document.getElementById("loginPage");
const mainPage = document.getElementById("mainPage");

const mainModalContent = document.getElementById('modal-main-content');

let userMetadata = [];

function showLoginPage() {
  welcomePage.style.display = "none";
  mainPage.style.display = "none";
  loginPage.style.display = "block";
}

function showMainPage() {
  welcomePage.style.display = "none";
  mainPage.style.display = "block";
  loginPage.style.display = "none";
}

function showMainModal(md) {
  mainModalContent.innerHTML = `<pre>${md.Config}</pre>`;
  MicroModal.show('modal-main');
}

function updateCfgRows(_userMetadata) {
  userMetadata = _userMetadata;
  const cfgRows = document.getElementById('cfgRows');
  cfgRows.innerHTML = '';
  userMetadata.map((el, idx, _) => {
    const newEl = `
        <div>
          <span>${el.Name}</span>
          <span>${el.Plan}</span>
          <span>${el.DateTo}</span>
          <button class="button is-success main__showCfg" data-key=${idx}>Show</button>
        </div>`;
    cfgRows.insertAdjacentHTML('beforeend', newEl);

    const btn = cfgRows.querySelector(`button[data-key="${idx}"]`);
    btn.addEventListener('click', () => {
      showMainModal(userMetadata[idx]);
    });
  });
}

function updatePlanOptions(planConfigs = {}) {
  const planOptions = document.getElementById('planOptions');
  Object.entries(planConfigs).forEach(([key, value]) => {
    const capitalizedKey = key.slice(0, 1).toUpperCase() + key.slice(1);
    const newEl = `
          <label class="radio ml-0 mt-2">
            <input type="radio" name="question" data-plan="${key}" />
              <span class="main__label">
                ${capitalizedKey} (duration ${value.minutes} minutes, max count of config ${value.limit})
              </span>
          </label>`;
    planOptions.insertAdjacentHTML('beforeend', newEl);
  });

  const radio = document.querySelector('input[data-plan="common"]');
  radio.checked = true;
}

function updateEmail(email = "") {
  document.getElementById("email").innerText = email;
}

async function handleWelcome() {
  try {
    const idToken = await auth.currentUser.getIdToken(true);
    const response = await fetch('/welcome', {
      method: 'GET',
      headers: {
        'X-Auth-Token': idToken
      }
    });
    const resp = await response.json();

    console.log(resp);

    updatePlanOptions(resp.planConfigs);
    updateCfgRows(resp.userMetadata || []);
    updateEmail(resp.email);

    showMainPage();
  } catch (error) {
    console.error('Error:', error);
  }
}

async function handleSignOut() {
  try {
    await signOut(auth);
  } catch (error) {
    console.error(error);
  }
}

async function handleSignIn() {
  try {
    const result = await signInWithPopup(auth, provider);
    GoogleAuthProvider.credentialFromResult(result);
  } catch (error) {
    alert("error: " + error);
    // Handle Errors here.
    const errorCode = error.code;
    const errorMessage = error.message;
    // The email of the user's account used.
    const email = error.customData.email;
    // The AuthCredential type that was used.
    const credential = GoogleAuthProvider.credentialFromError(error);
    // ...
  }
}

async function handleNewPeer() {
  try {
    const idToken = await auth.currentUser.getIdToken(true);

    const cfgName = document.getElementById('cfgName').value;
    const planRadios = document.querySelectorAll('#planOptions input[type="radio"]');

    let selectedPlan;
    for (const radio of planRadios) {
      selectedPlan = radio.checked ? radio.dataset.plan : null;
    }

    const response = await fetch(`/newPeer?plan=${selectedPlan || "common"}&name=${cfgName || ""}`, {
      method: 'GET',
      headers: {
        'X-Auth-Token': idToken
      }
    });
    const data = await response.json();
    console.log('genQuery response ', data);

    // let resp =  {
    //   "Plan": "common",
    //   "Name": "333",
    //   "Config": "\n[Interface]\nPrivateKey = GD0ofTqDWIDCf+76d2Xr10oUg3gbtzRH7RBgJBxptVg=\nAddress = 10.8.0.6/29\nDNS = 1.1.1.1\n\n[Peer]\nPublicKey = +vYq1e8XgdnFD9f1e0GNB+RHZ0jk6ROCVlvOSM8+nVY=\nPresharedKey = wKVB/RGscjgu9J7Lnbv9ew26toJO06sK3tcX9OwKwnA=\nAllowedIPs = 0.0.0.0/0, ::/0\nPersistentKeepalive = 0\nEndpoint = localhost:51820\n",
    //   "DateTo": "2023-05-17T20:50:53.318948+03:00"
    // }

    document.getElementById('cfgName').value = "";

    userMetadata.unshift(data);
    updateCfgRows(userMetadata);
    showMainModal(data);
  } catch (error) {
    console.error('Error:', error);
  }
}

onAuthStateChanged(auth, user => {
  console.log("onAuthStateChanged");
  return user ? handleWelcome() : showLoginPage();
});

document.getElementById("googleSignIn").addEventListener("click", handleSignIn);
document.getElementById("googleSignOut").addEventListener("click", handleSignOut);
document.getElementById("newPeer").addEventListener("click", handleNewPeer);
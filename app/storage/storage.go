package storage

import (
	"context"
	"encoding/json"
	"log"
	"wg-go-web/model"

	"github.com/go-redis/redis/v8"
	"github.com/labstack/echo/v4"
)

type Storage interface {
	UserMetadata(ctx context.Context, uid string) ([]model.WgConfig, error)
	UpdateUserMetadata(ctx context.Context, uid string, md []model.WgConfig) error
}

type storage struct {
	redisClient *redis.Client
	logger      echo.Logger
}

func New(redisClient *redis.Client, logger echo.Logger) Storage {
	return &storage{
		redisClient: redisClient,
		logger:      logger,
	}
}

func redisUserMetadataKey(uid string) string {
	return "md-" + uid
}

func (c *storage) UserMetadata(ctx context.Context, uid string) ([]model.WgConfig, error) {
	val, err := c.redisClient.Get(ctx, redisUserMetadataKey(uid)).Result()
	if err != nil {
		if err == redis.Nil {
			return nil, nil
		}

		c.logger.Errorf("error getting user metadata for user %v: %v", uid, err)
		return nil, err
	}

	var md []model.WgConfig
	err = json.Unmarshal([]byte(val), &md)
	if err != nil {
		log.Printf("error core.UserMetadata, json.Unmarshal %v", err)
		return nil, err
	}

	return md, nil
}

func (c *storage) UpdateUserMetadata(ctx context.Context, uid string, md []model.WgConfig) error {
	mdJSON, err := json.Marshal(md)
	if err != nil {
		log.Printf("error core.UpdateUserMetadata, json.Marshal(md) %v", err)
		return err
	}

	err = c.redisClient.Set(ctx, redisUserMetadataKey(uid), mdJSON, 0).Err()
	if err != nil {
		log.Printf("error core.UpdateUserMetadata, redisClient.Set - mdJSON %v", err)
		return err
	}

	return nil
}

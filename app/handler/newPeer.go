package handler

import (
	"net/http"
	"time"
	"wg-go-web/model"

	"github.com/labstack/echo/v4"
)

func (h *handler) req2WgCore(duration int) (string, time.Time, error) {
	url := fmt.Sprintf("http://%s/newPeer?duration_in_minutes=%s", h.config.CoreAddr, strconv.Itoa(duration))

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		h.logger.Errorf("error creating request %v", err)
		return "", time.Time{}, err
	}
	req.Header.Set("X-API-Key", h.config.CoreAuthKey)

	resp, err := h.httpClient.Do(req)
	if err != nil {
		h.logger.Errorf("error creating response %v", err)
		return "", time.Time{}, err
	}
	defer resp.Body.Close()

	var parsedBody struct {
		Config string    `json:"config"`
		DateTo time.Time `json:"date_to"`
	}
	err = json.NewDecoder(resp.Body).Decode(&parsedBody)
	if err != nil {
		h.logger.Errorf("error decoding JSON response %v", err)
		return "", time.Time{}, err
	}

	return parsedBody.Config, parsedBody.DateTo, err

	// res := `
	// 	[Interface]
	// 	PrivateKey = GD0ofTqDWIDCf+76d2Xr10oUg3gbtzRH7RBgJBxptVg=
	// 	Address = 10.8.0.6/29
	// 	DNS = 1.1.1.1

	// 	[Peer]
	// 	PublicKey = +vYq1e8XgdnFD9f1e0GNB+RHZ0jk6ROCVlvOSM8+nVY=
	// 	PresharedKey = wKVB/RGscjgu9J7Lnbv9ew26toJO06sK3tcX9OwKwnA=
	// 	AllowedIPs = 0.0.0.0/0, ::/0
	// 	PersistentKeepalive = 0
	// 	Endpoint = localhost:51820
	// `

	// now := time.Now()
	// future := now.AddDate(0, 0, 3)

	// return res, future, nil
}

func (h *handler) NewPeer(c echo.Context) error {
	ctx := c.Request().Context()
	user := c.(*WithUserContext)

	name := c.QueryParam("name")
	if name == "" {
		name = h.ng.Generate()
	}

	plan := model.ToPlan(c.QueryParam("plan"))
	if plan == "" {
		return c.JSON(http.StatusBadRequest, "plan not specified")
	}

	planRule, ok := model.PlanConfigOptions[plan]
	if !ok {
		return c.JSON(http.StatusBadRequest, "plan not found")
	}

	md, err := h.storage.UserMetadata(ctx, user.Uid)
	if err != nil {
		h.logger.Errorf("can't get user metadata for user %v: %v", user, err)
		return c.JSON(http.StatusInternalServerError, ErrInternalResponse)
	}

	count := 0
	for _, config := range md {
		if config.Plan == plan {
			count += 1
		}
	}

	if count >= planRule.Limit {
		return c.JSON(http.StatusBadRequest, "per day limit error")
	}

	conf, dateTo, err := h.req2WgCore(planRule.DurationMin)
	if err != nil {
		h.logger.Errorf("can't get new config %v", err)
		return c.JSON(http.StatusInternalServerError, ErrInternalResponse)
	}

	md = append(md, model.WgConfig{
		Plan:   plan,
		Name:   name,
		Config: conf,
		DateTo: dateTo,
	})

	err = h.storage.UpdateUserMetadata(ctx, user.Uid, md)
	if err != nil {
		h.logger.Errorf("cannot update user metadata for user %v: %v", user.Uid, err)
		return c.JSON(http.StatusInternalServerError, ErrInternalResponse)
	}

	return c.JSON(http.StatusOK, model.WgConfig{
		Plan:   plan,
		Name:   name,
		Config: conf,
		DateTo: dateTo,
	})
}

package handler

import (
	"net/http"
	"time"
	"wg-go-web/model"

	"github.com/labstack/echo/v4"
)

/*
	curl http://localhost:3000/welcome
		-H "Content-Type: application/json"
		-d '{"uid":"yxgg8u6epDZPu5nMOgN8Elj8inc2","email":"kostyamarevtsev@gmail.com","displayName":"Константин Маревцев"}'
*/

type WelcomeResponse struct {
	UserEmail         string            `json:"email"`
	UserMetadata      []model.WgConfig  `json:"userMetadata"`
	PlanConfigOptions model.PlanConfigs `json:"planConfigs"`
}

func deleteExpiredConfigs(md []model.WgConfig) []model.WgConfig {
	now := time.Now().UTC()
	var res []model.WgConfig
	for _, config := range md {
		if !config.DateTo.Before(now) {
			res = append(res, config)
		}
	}
	return res
}

func (h *handler) Welcome(c echo.Context) error {
	ctx := c.Request().Context()
	user := c.(*WithUserContext)

	md, err := h.storage.UserMetadata(ctx, user.Uid)
	if err != nil {
		h.logger.Errorf("could not get user metadata for user %v: %v", user.Uid, err)
		return c.JSON(http.StatusInternalServerError, ErrInternalResponse)
	}

	md = deleteExpiredConfigs(md)

	err = h.storage.UpdateUserMetadata(ctx, user.Uid, md)
	if err != nil {
		h.logger.Errorf("cannot update user metadata %v", err)
	}

	return c.JSON(http.StatusOK, WelcomeResponse{
		UserEmail:         user.Email,
		UserMetadata:      md,
		PlanConfigOptions: model.PlanConfigOptions,
	})
}

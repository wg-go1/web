package handler

import (
	"net/http"
	"wg-go-web/config"
	"wg-go-web/storage"

	"firebase.google.com/go/auth"
	"github.com/goombaio/namegenerator"
	"github.com/labstack/echo/v4"
)

var ErrInternalResponse = echo.Map{
	"message": "Internal server error",
}

var ErrParseBody = echo.Map{
	"message": "Parse body error",
}

type handler struct {
	storage        storage.Storage
	httpClient     *http.Client
	firebaseClient *auth.Client
	logger         echo.Logger
	ng             namegenerator.Generator
	config         *config.Config
}

func New(storage storage.Storage, firebaseClient *auth.Client, logger echo.Logger, ng namegenerator.Generator, config *config.Config) *handler {
	httpClient := &http.Client{}
	return &handler{
		storage, httpClient, firebaseClient, logger, ng, config,
	}
}

type WithUserContext struct {
	Uid   string
	Email string
	echo.Context
}

func (h *handler) VerifyToken(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		idToken := c.Request().Header.Get("X-Auth-Token")
		authToken, err := h.firebaseClient.VerifyIDToken(c.Request().Context(), idToken)
		if err != nil {
			return c.JSON(http.StatusUnauthorized, nil)
		}

		claims := authToken.Claims

		email, ok := claims["email"].(string)
		if !ok && email == "" {
			return c.JSON(http.StatusUnauthorized, nil)
		}

		userID, ok := claims["user_id"].(string)
		if !ok && userID == "" {
			return c.JSON(http.StatusUnauthorized, nil)
		}

		cc := &WithUserContext{userID, email, c}
		return next(cc)
	}
}

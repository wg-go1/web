FROM node:19 AS frontend-builder
WORKDIR /app/frontend
COPY ./app/frontend .
RUN npm install && npm run deploy


FROM golang:1.17.5-alpine3.15 AS builder
WORKDIR /app
COPY ./app .
RUN apk --no-cache add ca-certificates && \
    CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o app .

FROM alpine:3.17
WORKDIR /app
COPY --from=builder /app/app /app
COPY --from=frontend-builder /app/frontend /app/frontend
EXPOSE 3000
CMD ["/app/app"]